# [front-end-odyssey]

## Quick Start

Install Node.js and then:

```sh
$ npm -g install grunt-cli bower
$ npm install
$ bower install
$ grunt
```

This will open http://localhost:1060/ in your browser displaying the sample application.
