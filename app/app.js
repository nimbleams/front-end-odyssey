var feoApp = angular.module('feoApp', []);

feoApp.controller('feoCtrl', function feoCtrl($scope, sessionApi){
  $scope.title = '2015: A Front End Odyssey';

  sessionApi.getAll().then(function(response) {
    $scope.sessions = response.data;
  });
});

feoApp.factory('sessionApi', function sessionApi($http) {
  var api = {
    getAll: function() {
      return $http.get('/data/sessions.json');
    }
  };

  return api;
});
